class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :event
      t.string :package
      t.string :capacity

      t.timestamps
    end
  end
end
